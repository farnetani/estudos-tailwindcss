# Instalação do TwailwindCSS

- Instalar via npm
- Criar o arquivo css que irá conter as diretivas do Tailwind;
- Por último precisamos buildar o arquivo baseado no arquivo de configurações criado, apontando para a saída que é o arquivo final de css;

```
npm i tailwindcss@latest

Rodar isso antes (bug): https://github.com/webpack-contrib/postcss-loader/issues/187
npm i -D autoprefixer@latest postcss@latest
```

Criar a pasta `src` e fora dela vamos criar o arquivo `styles.css`

```

@tailwind base;

@tailwind components;

@tailwind utilities;
```

Rodar o comando abaixo para criar o arquivo de configurações do tailwindcss:

```
npx tailwindcss init
```

Isso irá criar no root do projeto o arquivo `tawind.config.js`

```js
module.exports = {
  purge: [],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {},
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
```

No campo theme > extend é onde colocaremos os complementos (quase sempre usaremos).

Agora temos que buildar o tailwindcss:

```
# pode usar o mesmo nome do arquivo do root, mas esse arquivo será o buildado
npx tailwindcss-cli@latest build styles.css -o css/styles.css
```

Perceba que agora dentro do diretório `src/css` teremos o arquivo `styles.css` buildado pelo `twailwind.css`

Criar o arquivo `index.html`na pasta `src`

```html
<!DOCTYPE html>
<html lang="pt-BR">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  
  <!-- Referência do TwailWindCSS -->
  <link rel="stylesheet" href="css/styles.css">  
  
  <title>Estudos de TwailWindCSS</title>
</head>
<body>
  <h1 class="text-red-900 ml-20 mt-20 text-xl">Hello world Tailwind!</h1>  
</body>
</html>
```

Pressionando: `ctrl+shift+p` e Live Server: Open with Live Server para abrir o projeto no navegador com hot-reload!